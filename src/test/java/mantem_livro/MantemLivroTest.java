package mantem_livro;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;

import java.util.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

public class MantemLivroTest {
	private WebDriver driver;
	private Map<String, Object> vars;
	JavascriptExecutor js;

	@BeforeEach
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "browserDriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://ts-scel-web.herokuapp.com/login");
		driver.manage().window().maximize();
		js = (JavascriptExecutor) driver;
		vars = new HashMap<String, Object>();
	} 

	@AfterEach
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void ct01cadastrarlivrocomsucesso() {
        driver.findElement(By.name("username")).click();
        driver.findElement(By.name("username")).sendKeys("carlos");
        driver.findElement(By.name("password")).click();
        driver.findElement(By.name("password")).sendKeys("123");
        driver.findElement(By.cssSelector("button")).click();

        waiting(); 

        driver.findElement(By.linkText("Livros")).click();
        driver.findElement(By.id("isbn")).click();
        driver.findElement(By.id("isbn")).sendKeys("1100");
        driver.findElement(By.id("autor")).click();
        driver.findElement(By.id("autor")).sendKeys("Kent Beck");
        driver.findElement(By.id("titulo")).click();
        driver.findElement(By.id("titulo")).sendKeys("Extreme programming");
        driver.findElement(By.cssSelector(".btn:nth-child(1)")).click();
        driver.findElement(By.id("paginaConsulta")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));
        
        driver.findElement(By.linkText("Livros")).click();
        driver.findElement(By.id("isbn")).click();
        driver.findElement(By.id("isbn")).sendKeys("3011");
        driver.findElement(By.id("autor")).click();
        driver.findElement(By.id("autor")).sendKeys("Frederick Brooks");
        driver.findElement(By.id("titulo")).click();
        driver.findElement(By.id("titulo")).sendKeys("O mitico homem-mes");
        driver.findElement(By.cssSelector(".btn:nth-child(1)")).click();
        driver.findElement(By.id("paginaConsulta")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));

	}

	@Test
	public void ct02ConsultarLivro() {
        driver.findElement(By.name("username")).click();
        driver.findElement(By.name("username")).sendKeys("carlos");
        driver.findElement(By.name("password")).click();
        driver.findElement(By.name("password")).sendKeys("123");

        waiting(); 

        driver.findElement(By.linkText("Livros")).click();
        driver.findElement(By.id("isbn")).click();
        driver.findElement(By.id("isbn")).sendKeys("1100");
        driver.findElement(By.linkText("Lista de Livros")).click();
        driver.findElement(By.id("paginaConsulta")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));
        
        driver.findElement(By.linkText("Livros")).click();
        driver.findElement(By.id("isbn")).click();
        driver.findElement(By.id("isbn")).sendKeys("3011");
        driver.findElement(By.linkText("Lista de Livros")).click();
        driver.findElement(By.id("paginaConsulta")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));
        
	}

	@Test
	public void ct03EditarLivro() {

        driver.findElement(By.name("username")).click();
        driver.findElement(By.name("username")).sendKeys("carlos");
        driver.findElement(By.name("password")).click();
        driver.findElement(By.name("password")).sendKeys("123");
        driver.findElement(By.cssSelector("button")).click();

        waiting(); 

        driver.findElement(By.linkText("Livros")).click();
        driver.findElement(By.linkText("Lista de Livros")).click();
        driver.findElement(By.cssSelector("body")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));

        driver.findElement(By.cssSelector("tr:nth-child(3) .btn-primary")).click();
        driver.findElement(By.id("titulo")).click();
        driver.findElement(By.id("titulo")).sendKeys("O mitico homem-mes");
        driver.findElement(By.id("isbn")).click();
        driver.findElement(By.id("isbn")).sendKeys("3334");
        driver.findElement(By.cssSelector(".btn")).click();
        driver.findElement(By.cssSelector("body")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));
	}

	@Test
	public void ct04excluirlivrocomsucesso() {
        driver.findElement(By.name("username")).click();
        driver.findElement(By.name("username")).sendKeys("carlos");
        driver.findElement(By.name("password")).click();
        driver.findElement(By.name("password")).sendKeys("123");
        driver.findElement(By.cssSelector("button")).click();

        waiting(); 

        driver.findElement(By.linkText("Livros")).click();
        driver.findElement(By.cssSelector(".my-5")).click();
        assertThat(driver.findElement(By.cssSelector("h3:nth-child(2)")).getText(), is("Cadastrar Livro"));

        driver.findElement(By.linkText("Lista de Livros")).click();
        driver.findElement(By.id("paginaConsulta")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));

        driver.findElement(By.cssSelector("tr:nth-child(3) .delete")).click();
        driver.findElement(By.cssSelector("body")).click();
        assertThat(driver.findElement(By.id("paginaConsulta")).getText(), is("Lista de livros"));
	}

    public void waiting() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String waitForWindow(int timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Set<String> whNow = driver.getWindowHandles();
        Set<String> whThen = (Set<String>) vars.get("window_handles");
        
        if (whNow.size() > whThen.size()) {
            whNow.removeAll(whThen);
        }
        
        return whNow.iterator().next();
    }
}



